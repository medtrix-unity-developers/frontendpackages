$(function () {
    $(".mapcontainer").mapael({
        map: {
            name: "world_countries",
            defaultArea: {
                attrs: {
                    stroke: "#fff",
                    "stroke-width": 0
                }
            }
        },
        legend: {
            area: {
                mode: "horizontal",
                // exclusive: true,
                title: "",
                labelAttrs: {
                    "font-size": 12
                },
                marginLeft: 5,
                marginLeftLabel: 5,
                slices: [
                    {
                        max: 0,
                        attrs: {
                            fill: "#E5E5E5"
                        },
                        label: "No Data"
                    },
                    {
                        min: 1,
                        max: 4,
                        attrs: {
                            fill: "#48BED8"
                        },
                        label: "1-4%"
                    },
                    {
                        min: 5,
                        max: 9,
                        attrs: {
                            fill: "#008AAF"
                        },
                        label: "5-9%"
                    },
                    {
                        min: 10,
                        max: 14,
                        attrs: {
                            fill: "#2F4497"
                        },
                        label: "10-14%"
                    },
                    {
                        min: 15,
                        max: 19,
                        attrs: {
                            fill: "#6F5BCB"
                        },
                        label: "15-19%"
                    },
                    {
                        min: 20,
                        max: 24,
                        attrs: {
                            fill: "#EB65DB"
                        },
                        label: "20-24%"
                    },
                    {
                        min: 35,
                        max: 39,
                        attrs: {
                            fill: "#DFA70F"
                        },
                        label: "35-39%"
                    },
                    {
                        min: 40,
                        max: 44,
                        attrs: {
                            fill: "#E04308"
                        },
                        label: "40-44%"
                    },
                    {
                        min: 45,
                        max: 49,
                        attrs: {
                            fill: "#980101"
                        },
                        label: "45-49%"
                    }
                ]
            }
        },
        
        areas: {
            "GL": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Greenland(Denmark)<\/span><br \/>Percentage : No data"
                }
            },
            "IS": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Iceland<\/span><br \/>Percentage : No data"
                }
            },
            "AF": {
                "value": "0",
                "attrs": {
                //"href": "#"
                },
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Afghanistan<\/span><br \/>Percentage : No data"
                }
            },
            "ZA": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">South Africa<\/span><br \/>Percentage : No data"
                }
            },
            "AL": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Albania<\/span><br \/>Percentage : No data"
                }
            },
            "AD": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Andorra<\/span><br \/>Percentage : No data"
                }
            },
            "AO": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Angola<\/span><br \/>Percentage : No data"
                }
            },
            "AG": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Antigua And Barbuda<\/span><br \/>Percentage : No data"
                }
            },
            "SA": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Saudi Arabia<\/span><br \/>Percentage : No data"
                }
            },
            "AR": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Argentina<\/span><br \/>Percentage : No data"
                }
            },
            "AM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Armenia<\/span><br \/>Percentage : No data"
                }
            },
            "AT": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Austria<\/span><br \/>Percentage : No data"
                }
            },
            "AZ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Azerbaijan<\/span><br \/>Percentage : No Data"
                }
            },
            "BS": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Bahamas<\/span><br \/>Percentage : No Data"
                }
            },
            "BH": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Bahrain<\/span><br \/>Percentage : No Data"
                }
            },
            "BB": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Barbados<\/span><br \/>Percentage : No Data"
                }
            },
            "BE": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Belgium<\/span><br \/>Percentage : No Data"
                }
            },
            "BZ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Belize<\/span><br \/>Percentage : No Data"
                }
            },
            "BJ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Benin<\/span><br \/>Percentage : No Data"
                }
            },
            "BT": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Bhutan<\/span><br \/>Percentage : No Data"
                }
            },
            "BY": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Belarus<\/span><br \/>Percentage : No Data"
                }
            },
            "MM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Myanmar<\/span><br \/>Percentage : No Data"
                }
            },
            "BO": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Bolivia, Plurinational State Of<\/span><br \/>Percentage : No Data"
                }
            },
            "BA": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Bosnia And Herzegovina<\/span><br \/>Percentage : No Data"
                }
            },
            "BW": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Botswana<\/span><br \/>Percentage : No Data"
                }
            },
            "BN": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Brunei Darussalam<\/span><br \/>Percentage : No Data"
                }
            },
            "BG": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Bulgaria<\/span><br \/>Percentage : No Data"
                }
            },
            "BF": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Burkina Faso<\/span><br \/>Percentage : No Data"
                }
            },
            "BI": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Burundi<\/span><br \/>Percentage : No Data"
                }
            },
            "KH": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Cambodia<\/span><br \/>Percentage : No Data"
                }
            },
            "CM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Cameroon<\/span><br \/>Percentage : No Data"
                }
            },
            "CV": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Cape Verde<\/span><br \/>Percentage : No Data"
                }
            },
            "CF": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Central African Republic<\/span><br \/>Percentage : No Data"
                }
            },
            "CL": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Chile<\/span><br \/>Percentage : No Data"
                }
            },
            "CY": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Cyprus<\/span><br \/>Percentage : No Data"
                }
            },
            "CO": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Colombia<\/span><br \/>Percentage : No Data"
                }
            },
            "KM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Comoros<\/span><br \/>Percentage : No Data"
                }
            },
            "CG": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Congo<\/span><br \/>Percentage : No Data"
                }
            },
            "CD": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Congo, The Democratic Republic Of The<\/span><br \/>Percentage : No Data"
                }
            },
            "KP": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Korea, Democratic People's Republic Of<\/span><br \/>Percentage : No Data"
                }
            },
            "CR": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Costa Rica<\/span><br \/>Percentage : No Data"
                }
            },
            "CI": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">C\u00d4te D'ivoire<\/span><br \/>Percentage : No Data"
                }
            },
            "HR": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Croatia<\/span><br \/>Percentage : No Data"
                }
            },
            "CU": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Cuba<\/span><br \/>Percentage : No Data"
                }
            },
            "DK": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Denmark<\/span><br \/>Percentage : No Data"
                }
            },
            "DJ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Djibouti<\/span><br \/>Percentage : No Data"
                }
            },
            "DM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Dominica<\/span><br \/>Percentage : No Data"
                }
            },
            "EG": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Egypt<\/span><br \/>Percentage : No Data"
                }
            },
            "AE": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">United Arab Emirates<\/span><br \/>Percentage : No Data"
                }
            },
            "ER": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Eritrea<\/span><br \/>Percentage : No Data"
                }
            },
            "EE": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Estonia<\/span><br \/>Percentage : No Data"
                }
            },
            "ET": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Ethiopia<\/span><br \/>Percentage : No Data"
                }
            },
            "FJ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Fiji<\/span><br \/>Percentage : No Data"
                }
            },
            "FI": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Finland<\/span><br \/>Percentage : No Data"
                }
            },
            "GA": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Gabon<\/span><br \/>Percentage : No Data"
                }
            },
            "GM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Gambia<\/span><br \/>Percentage : No Data"
                }
            },
            "GE": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Georgia<\/span><br \/>Percentage : No Data"
                }
            },
            "GH": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Ghana<\/span><br \/>Percentage : No Data"
                }
            },
            "GR": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Greece<\/span><br \/>Percentage : No Data"
                }
            },
            "GD": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Grenada<\/span><br \/>Percentage : No Data"
                }
            },
            "GT": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Guatemala<\/span><br \/>Percentage : No Data"
                }
            },
            "GN": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Guinea<\/span><br \/>Percentage : No Data"
                }
            },
            "GQ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Equatorial Guinea<\/span><br \/>Percentage : No Data"
                }
            },
            "GW": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Guinea-bissau<\/span><br \/>Percentage : No Data"
                }
            },
            "GY": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Guyana<\/span><br \/>Percentage : No Data"
                }
            },
            "HT": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Haiti<\/span><br \/>Percentage : No Data"
                }
            },
            "HN": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Honduras<\/span><br \/>Percentage : No Data"
                }
            },
            "JM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Jamaica<\/span><br \/>Percentage : No Data"
                }
            },
            "MH": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Marshall Islands<\/span><br \/>Percentage : No Data"
                }
            },
            "PW": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Palau<\/span><br \/>Percentage : No Data"
                }
            },
            "SB": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Solomon Islands<\/span><br \/>Percentage : No Data"
                }
            },
            
            "JO": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Jordan<\/span><br \/>Percentage : No Data"
                }
            },
            "IQ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Iraq<\/span><br \/>Percentage : No Data"
                }
            },
            "IE": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Ireland<\/span><br \/>Percentage : No Data"
                }
            },
            "IL": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Israel<\/span><br \/>Percentage : No Data"
                }
            },
            "KZ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Kazakhstan<\/span><br \/>Percentage : No Data"
                }
            },
            "KE": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Kenya<\/span><br \/>Percentage : No Data"
                }
            },
            "KG": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Kyrgyzstan<\/span><br \/>Percentage : No Data"
                }
            },
            "KI": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Kiribati<\/span><br \/>Percentage : No Data"
                }
            },
            "KW": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Kuwait<\/span><br \/>Percentage : No Data"
                }
            },
            "LA": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Lao People's Democratic Republic<\/span><br \/>Percentage : No Data"
                }
            },
            "LS": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Lesotho<\/span><br \/>Percentage : No Data"
                }
            },
            "LV": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Latvia<\/span><br \/>Percentage : No Data"
                }
            },
            "LB": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Lebanon<\/span><br \/>Percentage : No Data"
                }
            },
            "LR": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Liberia<\/span><br \/>Percentage : No Data"
                }
            },
            "LY": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Libya<\/span><br \/>Percentage : No Data"
                }
            },
            "LI": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Liechtenstein<\/span><br \/>Percentage : No Data"
                }
            },
            "LT": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Lithuania<\/span><br \/>Percentage : No Data"
                }
            },
            "LU": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Luxembourg<\/span><br \/>Percentage : No Data"
                }
            },
            "MK": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Macedonia, The Former Yugoslav Republic Of<\/span><br \/>Percentage : No Data"
                }
            },
            "MG": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Madagascar<\/span><br \/>Percentage : No Data"
                }
            },
            "MW": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Malawi<\/span><br \/>Percentage : No Data"
                }
            },
            "MV": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Maldives<\/span><br \/>Percentage : No Data"
                }
            },
            "ML": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Mali<\/span><br \/>Percentage : No Data"
                }
            },
            "MT": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Malta<\/span><br \/>Percentage : No Data"
                }
            },
            "MA": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Morocco<\/span><br \/>Percentage : No Data"
                }
            },
            "MU": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Mauritius<\/span><br \/>Percentage : No Data"
                }
            },
            "MR": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Mauritania<\/span><br \/>Percentage : No Data"
                }
            },
            "FM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Micronesia, Federated States Of<\/span><br \/>Percentage : No Data"
                }
            },
            "MD": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Moldova, Republic Of<\/span><br \/>Percentage : No Data"
                }
            },
            "MC": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Monaco<\/span><br \/>Percentage : No Data"
                }
            },
            "MN": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Mongolia<\/span><br \/>Percentage : No Data"
                }
            },
            "ME": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Montenegro<\/span><br \/>Percentage : No Data"
                }
            },
            "MZ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Mozambique<\/span><br \/>Percentage : No Data"
                }
            },
            "NA": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Namibia<\/span><br \/>Percentage : No Data"
                }
            },
            "NP": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Nepal<\/span><br \/>Percentage : No Data"
                }
            },
            "NI": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Nicaragua<\/span><br \/>Percentage : No Data"
                }
            },
            "NE": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Niger<\/span><br \/>Percentage : No Data"
                }
            },
            "NG": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Nigeria<\/span><br \/>Percentage : No Data"
                }
            },
            "NZ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">New Zealand<\/span><br \/>Percentage : No Data"
                }
            },
            "OM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Oman<\/span><br \/>Percentage : No Data"
                }
            },
            "UG": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Uganda<\/span><br \/>Percentage : No Data"
                }
            },
            "UZ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Uzbekistan<\/span><br \/>Percentage : No Data"
                }
            },
            "PK": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Pakistan<\/span><br \/>Percentage : No Data"
                }
            },
            "PS": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Palestine, State Of<\/span><br \/>Percentage : No Data"
                }
            },
            "PA": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Panama<\/span><br \/>Percentage : No Data"
                }
            },
            "PY": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Paraguay<\/span><br \/>Percentage : No Data"
                }
            },
            "NL": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Netherlands<\/span><br \/>Percentage : No Data"
                }
            },
            "PH": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Philippines<\/span><br \/>Percentage : No Data"
                }
            },
            "PL": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Poland<\/span><br \/>Percentage : No Data"
                }
            },
            "QA": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Qatar<\/span><br \/>Percentage : No Data"
                }
            },
            "DO": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Dominican Republic<\/span><br \/>Percentage : No Data"
                }
            },
            "RO": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Romania<\/span><br \/>Percentage : No Data"
                }
            },
            "RU": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Russian Federation<\/span><br \/>Percentage : No Data"
                }
            },
            "RW": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Rwanda<\/span><br \/>Percentage : No Data"
                }
            },
            "KN": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Saint Kitts And Nevis<\/span><br \/>Percentage : No Data"
                }
            },
            "SM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">San Marino<\/span><br \/>Percentage : No Data"
                }
            },
            "VC": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Saint Vincent And The Grenadines<\/span><br \/>Percentage : No Data"
                }
            },
            "LC": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Saint Lucia<\/span><br \/>Percentage : No Data"
                }
            },
            "SV": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">El Salvador<\/span><br \/>Percentage : No Data"
                }
            },
            "WS": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Samoa<\/span><br \/>Percentage : No Data"
                }
            },
            "ST": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Sao Tome And Principe<\/span><br \/>Percentage : No Data"
                }
            },
            "SN": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Senegal<\/span><br \/>Percentage : No Data"
                }
            },
            "RS": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Serbia<\/span><br \/>Percentage : No Data"
                }
            },
            "SC": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Seychelles<\/span><br \/>Percentage : No Data"
                }
            },
            "SL": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Sierra Leone<\/span><br \/>Percentage : No Data"
                }
            },
            "SG": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Singapore<\/span><br \/>Percentage : No Data"
                }
            },
            "SK": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Slovakia<\/span><br \/>Percentage : No Data"
                }
            },
            "SI": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Slovenia<\/span><br \/>Percentage : No Data"
                }
            },
            "SO": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Somalia<\/span><br \/>Percentage : No Data"
                }
            },
            "SD": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Sudan<\/span><br \/>Percentage : No Data"
                }
            },
            "SS": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">South Sudan<\/span><br \/>Percentage : No Data"
                }
            },
            "CH": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Switzerland<\/span><br \/>Percentage : No Data"
                }
            },
            "SR": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Suriname<\/span><br \/>Percentage : No Data"
                }
            },
            "SZ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Swaziland<\/span><br \/>Percentage : No Data"
                }
            },
            "SY": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Syrian Arab Republic<\/span><br \/>Percentage : No Data"
                }
            },
            "TJ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Tajikistan<\/span><br \/>Percentage : No Data"
                }
            },
            "TZ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Tanzania, United Republic Of<\/span><br \/>Percentage : No Data"
                }
            },
            "TD": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Chad<\/span><br \/>Percentage : No Data"
                }
            },
            "CZ": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Czech Republic<\/span><br \/>Percentage : No Data"
                }
            },
            "TL": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Timor-leste<\/span><br \/>Percentage : No Data"
                }
            },
            "TG": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Togo<\/span><br \/>Percentage : No Data"
                }
            },
            "TO": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Tonga<\/span><br \/>Percentage : No Data"
                }
            },
            "TT": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Trinidad And Tobago<\/span><br \/>Percentage : No Data"
                }
            },
            "TN": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Tunisia<\/span><br \/>Percentage : No Data"
                }
            },
            "TM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Turkmenistan<\/span><br \/>Percentage : No Data"
                }
            },
            
            "TV": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Tuvalu<\/span><br \/>Percentage : No Data"
                }
            },
            "VU": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Vanuatu<\/span><br \/>Percentage : No Data"
                }
            },
            "UA": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Ukraine<\/span><br \/>Percentage : No Data"
                }
            },
            "UY": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Uruguay<\/span><br \/>Percentage : No Data"
                }
            },
            "YE": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Yemen<\/span><br \/>Percentage : No Data"
                }
            },
            "ZM": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Zambia<\/span><br \/>Percentage : No Data"
                }
            },
            "ZW": {
                "value": "0",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Zimbabwe<\/span><br \/>Percentage : No Data"
                }
            },
            "PE": {
                "value": "1",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Peru<\/span><br \/>Percentage : 1-4%"
                }
            },
            "BR": {
                "value": "1",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Brazil<\/span><br \/>Percentage : 1-4%"
                }
            },
            "AU": {
                "value": "1",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Australia<\/span><br \/>Percentage : 1-4%"
                }
            },
            "EC": {
                "value": "5",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Ecuador<\/span><br \/>Percentage : 5-9%"
                }
            },
            "VE": {
                "value": "5",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Venezuela, Bolivarian Republic Of<\/span><br \/>Percentage : 5-9%"
                }
            },
            "PT": {
                "value": "5",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Portugal<\/span><br \/>Percentage : 5-9%"
                }
            },
            "FR": {
                "value": "5",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">France<\/span><br \/>Percentage : 5-9%"
                }
            },
            "NO": {
                "value": "5",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Norway<\/span><br \/>Percentage : 5-9%"
                }
            },
            "BD": {
                "value": "5",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Bangladesh<\/span><br \/>Percentage : 5-9%"
                }
            },
            "US": {
                "value": "10",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">United States<\/span><br \/>Percentage : 10-14%"
                }
            },
            "DE": {
                "value": "10",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Germany<\/span><br \/>Percentage : 10-14%"
                }
            },
            "TR": {
                "value": "10",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Turkey<\/span><br \/>Percentage : 10-14%"
                }
            },
            "ID": {
                "value": "10",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Indonesia<\/span><br \/>Percentage : 10-14%"
                }
            },
            "MY": {
                "value": "10",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Malaysia<\/span><br \/>Percentage : 10-14%"
                }
            },
            "PG": {
                "value": "10",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Papua New Guinea<\/span><br \/>Percentage : 10-14%"
                }
            },
            "CA": {
                "value": "15",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Canada<\/span><br \/>Percentage : 15-19%"
                }
            },
            "GB": {
                "value": "15",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">United Kingdom<\/span><br \/>Percentage : 15-19%"
                }
            },
            "ES": {
                "value": "15",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Spain<\/span><br \/>Percentage : 15-19%"
                }
            },
            "IT": {
                "value": "15",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Italy<\/span><br \/>Percentage : 15-19%"
                }
            },
            "HU": {
                "value": "15",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Hungary<\/span><br \/>Percentage : 15-19%"
                }
            },
            "IR": {
                "value": "15",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Iran, Islamic Republic Of<\/span><br \/>Percentage : 15-19%"
                }
            },
            "LK": {
                "value": "15",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Sri Lanka<\/span><br \/>Percentage : 15-19%"
                }
            },
            "VN": {
                "value": "15",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Vietnam<\/span><br \/>Percentage : 15-19%"
                }
            },
            "MX": {
                "value": "20",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Mexico<\/span><br \/>Percentage : 20-24%"
                }
            },
            "SE": {
                "value": "20",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Sweden<\/span><br \/>Percentage : 20-24%"
                }
            },
            "DZ": {
                "value": "20",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Algeria<\/span><br \/>Percentage : 20-24%"
                }
            },
            "IN": {
                "value": "20",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">India<\/span><br \/>Percentage : 20-24%"
                }
            },
            "CN": {
                "value": "20",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">China<\/span><br \/>Percentage : 20-24%"
                }
            },
            "KR": {
                "value": "35",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Korea, Republic Of<\/span><br \/>Percentage : 35-39%"
                }
            },
            "TH": {
                "value": "40",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Thailand<\/span><br \/>Percentage : 40-44%"
                }
            },
            "JP": {
                "value": "45",
                //"href": "#",               
                "tooltip": {
                    "content": "<span style=\"font-weight:bold;\">Japan<\/span><br \/>Percentage : 45-49%"
                }
            }  
        }
    });
});