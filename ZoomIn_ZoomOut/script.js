let zoomArr = [0.9, 1.7, 2, 2.5];


var element = document.querySelector('.maindiv');
let value = element.getBoundingClientRect().width / element.offsetWidth;

let indexofArr = 1;
handleChange = () => {
  let val = document.querySelector('#sel').value;
  val = Number(val);
  console.log('handle change selected value ', val);
  indexofArr = zoomArr.indexOf(val);
  console.log('Handle changes', indexofArr);
  document.getElementById('Content').style.fontSize = `${val}vw`;
};



document.querySelector('.zoomin').addEventListener('click', () => {
  console.log('value of index zoomin is', indexofArr);
  if (indexofArr < zoomArr.length - 1) {
    indexofArr += 1;
    value = zoomArr[indexofArr];
    document.querySelector('#sel').value = value;
    document.getElementById('Content').style.fontSize = `${value}vw`;
  }
});

document.querySelector('.zoomout').addEventListener('click', () => {
  console.log('value of index  zoom out is', indexofArr);
  if (indexofArr > 0) {
    indexofArr -= 1;
    value = zoomArr[indexofArr];
    document.querySelector('#sel').value = value;
    document.getElementById('Content').style.fontSize = `${value}vw`;
  }
});