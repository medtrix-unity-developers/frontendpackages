$(function () {
  $('.pop-up').hide();
  $('#overlay').removeClass('blur-in');

  $('.content').click(function () {
    $('.pop-up').hide();
    $('.pop-up').fadeIn(1000);
    $('#overlay').addClass('blur-in');
    $('#overlay').removeClass('blur-out');
  });
  $('.close-button').click(function (e) {
    $('.pop-up').fadeOut(700);
    $('#overlay').removeClass('blur-in');
    $('#overlay').addClass('blur-out');
    e.stopPropagation();
  });
});

function myFunction() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}